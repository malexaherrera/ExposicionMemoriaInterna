package com.example.pc.accessinternalexternalmemory;


import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {
    Button buttonCrear, buttonAbrir;
    EditText editTextArchivo, editTextDatos;
    TextView textViewMostar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonCrear= findViewById(R.id.btnGuardar);
        buttonAbrir= findViewById(R.id.btnAbrir);
        editTextArchivo = findViewById(R.id.txtArchivo);
        editTextDatos = findViewById(R.id.txtDatos);
        textViewMostar = findViewById(R.id.lblMostrar);

        buttonCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearArcivo();
            }
        });
        buttonAbrir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                leerArchivo();
            }
        });


    }
    public  void crearArcivo(){
        String archivo = editTextArchivo.getText().toString();
        String datos = editTextDatos.getText().toString();
        try
        {
            OutputStreamWriter fout= new OutputStreamWriter(openFileOutput(archivo+".txt", Context.MODE_PRIVATE));
            fout.write(datos);
            fout.close();
            Toast.makeText(MainActivity.this, "Archivo Creado", Toast.LENGTH_SHORT).show();
        }
        catch (Exception ex)
        {
            Toast.makeText(MainActivity.this, "Error al escribir fichero a memoria interna", Toast.LENGTH_SHORT).show();
            Log.e("Ficheros", "Error al escribir fichero a memoria interna");
        }
    }
    public void leerArchivo(){
        String archivo = editTextArchivo.getText().toString();

        try
        {
            BufferedReader fin =new BufferedReader(new InputStreamReader(openFileInput(archivo+".txt")));
            textViewMostar.setText(fin.readLine());
            fin.close();
        }
        catch (Exception ex)
        {
            Toast.makeText(MainActivity.this, "Archivo no Encontrado", Toast.LENGTH_SHORT).show();
            Log.e("Ficheros", "Error al leer fichero desde memoria interna");
        }
    }
}